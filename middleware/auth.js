const jwt = require('jsonwebtoken');
const config = require('config');

//Una funcion middleware es basicamente una funcion que tiene acceso al request y response, y next es un callback que se debe hacer para avanzar a la siguiente etapa
module.exports = function (req, res, next) {
    //Obtener el token del header
    const token = req.header('x-auth-token');
    //Verificar si no hay token
    if (!token) {
        return res.status(401).json({ msg: 'No hay token. Acceso denegado.' });
    }

    //Verificar el token
    try {
        const decoded = jwt.verify(token, config.get('jwtSecret'));
        req.user = decoded.user;
        next();
    } catch (e) {
        res.status(401).json({ msg: 'Token inválido.' });
    }
}