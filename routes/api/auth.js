const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const { check, validationResult } = require('express-validator');

const User = require('../../models/User');

//@route    GET api/auth
//@desc     Test route
//@access   Public

//Al agregar auth como el segundo parametro, esta ruta queda protegida
router.get('/', auth, async (req, res) => {
    try {
        const user = await User.findById(req.user.id).select('-password');
        res.json(user);
    } catch (e) {
        console.log(e.message);
        res.status(500).send('Error de servidor.');
    }
});

//@route    POST api/auth
//@desc     Autenticar usuario y obtener el token
//@access   Public

//Copia del metodo POST de users.js, modificado para verificar si el usuario existe, no para crearlo
router.post('/', [
    //Validaciones
    check('email', 'Mail inválido.').isEmail(),
    check('password', 'Se necesita una contraseña.').exists()
], async (req, res) => {
    const errors = validationResult(req);
    //Si hubieron errores en las validaciones:
    if (!errors.isEmpty()) {
        //Se envía un Bad Request Error (400). Se entrega en JSON para que sea visible en el response
        return res.status(400).json({ errors: errors.array() });
    }

    const { email, password } = req.body;

    try {
        let user = await User.findOne({ email });

        //Si no hay un usuario, error
        if (!user) {
            res.status(400).json({ errors: [{ msg: 'Usuario o contraseña inválidos.' }] });
        }

        const isMatch = await bcrypt.compare(password, user.password);

        //Si no hay un usuario con esas credenciales, error
        if (!isMatch) {
            res.status(400).json({ errors: [{ msg: 'Usuario o contraseña inválidos.' }] });
        }

        //Devolver el JWT
        const payload = {
            //Se envia el id del usuario que fue guardado. Este id se obtiene del Promise que esta devolviendo user.save
            user: {
                //Se puede llamar con user.id gracias a Mongoose
                id: user.id
            }
        };

        //Se entrega a sign(): el id del usuario, el Secret de encriptacion (creo), el tiempo antes que expire el token (en segundos) y el callback (que puede ser un error, o un token. Si hay error, lo lanza, si es un token, le envia el token al cliente)
        jwt.sign(payload, config.get('jwtSecret'), { expiresIn: 3600 }, (err, token) => {
            if (err) throw err;
            res.json({ token });
        });
    } catch (e) {
        console.log(e.message);
        res.status(500).send('Error de servidor');
    }
});

module.exports = router;