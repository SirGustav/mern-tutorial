const express = require('express');
const router = express.Router();
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const { check, validationResult } = require('express-validator');

const User = require('../../models/User');

//@route    POST api/users
//@desc     Registrar usuario
//@access   Public
router.post('/', [
    //Validaciones
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    check('email', 'Por favor, escriba un mail válido').isEmail(),
    check('password', 'Por favor, ingrese una contraseña de 6 o más caracteres').isLength({ min: 6 })
], async (req, res) => {
    const errors = validationResult(req);
    //Si hubieron errores en las validaciones:
    if (!errors.isEmpty()) {
        //Se envía un Bad Request Error (400). Se entrega en JSON para que sea visible en el response
        return res.status(400).json({ errors: errors.array() });
    }

    //Se obtiene solo lo necesario del body
    const { name, email, password } = req.body;

    try {
        let user = await User.findOne({ email });

        //Si el usuario existe, error
        if (user) {
            //Bad Request si encuentra un usuario ya creado con el mismo correo
            res.status(400).json({ errors: [{ msg: 'El usuario ya existe' }] });
        }

        //Obtener el gravatar de los usuarios
        const avatar = gravatar.url(email, {
            s: '200', //Tamano de la imagen
            r: 'pg', //Rating, PG (Parental Guidance). Nada de desnudos ni sangre
            d: 'mm' //Imagen default (mm es de una silueta)
        });

        user = new User({
            name, email, avatar, password
        });

        //Encriptar contraseña, mientras mas grande el numero, mas segura, pero mas lento el proceso. 10 es el recomendado
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(password, salt);

        //Todo lo que retorne una Promise, se debe usar await, por las buenas practicas de async/await
        //Antes se hacia con un .then() , pero se anidaban demasiados [.then().then().then().then()]
        await user.save();

        //Devolver el JWT
        const payload = {
            //Se envia el id del usuario que fue guardado. Este id se obtiene del Promise que esta devolviendo user.save
            user: {
                //Se puede llamar con user.id gracias a Mongoose
                id: user.id
            }
        };

        //Se entrega a sign(): el id del usuario, el Secret de encriptacion (creo), el tiempo antes que expire el token (en segundos) y el callback (que puede ser un error, o un token. Si hay error, lo lanza, si es un token, le envia el token al cliente)
        jwt.sign(payload, config.get('jwtSecret'), { expiresIn: 3600 }, (err, token) => {
            if (err) throw err;
            res.json({ token });
        });
    } catch (e) {
        console.log(e.message);
        res.status(500).send('Error de servidor');
    }
});

module.exports = router;