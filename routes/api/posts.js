const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const auth = require('../../middleware/auth');

const Post = require('../../models/Post');
const Profile = require('../../models/Profile');
const User = require('../../models/User');

//@route    POST api/posts
//@desc     Crear un post
//@access   Private
router.post('/', [auth, [
    check('text', 'Es necesario un texto.').not().isEmpty()
]], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    try {
        const user = await User.findById(req.user.id).select('-password');

        const newPost = new Post({
            text: req.body.text,
            name: user.name,
            avatar: user.avatar,
            user: req.user.id
        });

        const post = await newPost.save();

        res.json(post);
    } catch (e) {
        console.error(e.message);
        res.status(500).send('Error de servidor.');
    }
});

//@route    GET api/posts
//@desc     Obtener todos los posts
//@access   Private
router.get('/', auth, async (req, res) => {
    try {
        //-1 los ordena desde el mas reciente en fecha
        const posts = await Post.find().sort({ date: -1 });
        res.json(posts);
    } catch (e) {
        console.error(e.message);
        res.status(500).send('Error de servidor.');
    }
});

//@route    GET api/posts/:id
//@desc     Obtener un post por id
//@access   Private
router.get('/:id', auth, async (req, res) => {
    try {
        const post = await Post.findById(req.params.id);

        if (!post) {
            return res.status(404).json({ msg: 'Post no encontrado.' });
        }

        res.json(post);
    } catch (e) {
        console.error(e.message);
        if (e.kind === 'ObjectId') {
            return res.status(404).json({ msg: 'Post no encontrado.' });
        }
        res.status(500).send('Error de servidor.');
    }
});

//@route    DELETE api/posts/:id
//@desc     Borrar un post por id
//@access   Private
router.delete('/:id', auth, async (req, res) => {
    try {
        const post = await Post.findById(req.params.id);

        if (!post) {
            return res.status(404).json({ msg: 'Post no encontrado.' });
        }

        //Verificar que el usuario que borra el post sea el mismo que lo creo
        //Se pasa a toString porque user es un ObjectId
        if (post.user.toString() !== req.user.id) {
            //401: No autorizado
            return res.status(401).json({ msg: 'Usuario no autorizado.' });
        }

        await post.remove();

        res.json({ msg: 'Post eliminado.' });
    } catch (e) {
        console.error(e.message);
        if (e.kind === 'ObjectId') {
            return res.status(404).json({ msg: 'Post no encontrado.' });
        }
        res.status(500).send('Error de servidor.');
    }
});

//@route    PUT api/posts/like/:id
//@desc     Dar like a un post
//@access   Private
router.put('/like/:id', auth, async (req, res) => {
    try {
        const post = await Post.findById(req.params.id);

        //Verificar si el post ya fue dado like por ese usuario
        if (post.likes.filter(like => like.user.toString() === req.user.id).length > 0) {
            return res.status(400).json({ msg: 'Ya fue dado un like con este usuario.' });
        }

        post.likes.unshift({ user: req.user.id });

        await post.save();

        res.json(post.likes);
    } catch (e) {
        console.error(e.message);
        res.status(500).send('Error de servidor.');
    }
});

//@route    PUT api/posts/unlike/:id
//@desc     Quitar like a un post
//@access   Private
router.put('/unlike/:id', auth, async (req, res) => {
    try {
        const post = await Post.findById(req.params.id);

        //Verificar si el post no tiene like, para mostrar error
        if (post.likes.filter(like => like.user.toString() === req.user.id).length === 0) {
            return res.status(400).json({ msg: 'Este post no tiene likes de este usuario.' });
        }

        //Obtener el remove index
        //Por cada like, se devuelve el usuario en forma de string
        const removeIndex = post.likes.map(like => like.user.toString()).indexOf(req.user.id);

        post.likes.splice(removeIndex, 1);

        await post.save();

        res.json(post.likes);
    } catch (e) {
        console.error(e.message);
        res.status(500).send('Error de servidor.');
    }
});

//@route    POST api/posts/comment/:id
//@desc     Comentar en un post
//@access   Private
router.post('/comment/:id', [auth, [
    check('text', 'Es necesario un texto.').not().isEmpty()
]], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    try {
        const user = await User.findById(req.user.id).select('-password');
        const post = await Post.findById(req.params.id);

        const newComment = {
            text: req.body.text,
            name: user.name,
            avatar: user.avatar,
            user: req.user.id
        };

        post.comments.unshift(newComment);

        await post.save();

        res.json(post.comments);
    } catch (e) {
        console.error(e.message);
        res.status(500).send('Error de servidor.');
    }
});

//@route    DELETE api/posts/comment/:id/:comment_id
//@desc     Borrar el comentario de un post
//@access   Private
router.delete('/comment/:id/:comment_id', auth, async (req, res) => {
    try {
        const post = await Post.findById(req.params.id);

        //Obtener el comentario
        const comment = post.comments.find(comment => comment.id === req.params.comment_id);

        //Asegurarse que el comentario exista
        if (!comment) {
            return res.status(404).json({ msg: 'El comentario no existe.' });
        }

        //Verificar que el usuario que borra el comentario es el que lo hizo
        if (comment.user.toString() !== req.user.id) {
            return res.status(401).json({ msg: 'Usuario no autorizado.' });
        }

        //Obtener el remove index
        //Por cada like, se devuelve el usuario en forma de string
        const removeIndex = post.comments.map(comment => comment.user.toString()).indexOf(req.user.id);

        post.comments.splice(removeIndex, 1);

        await post.save();

        res.json(post.comments);
    } catch (e) {
        console.error(e.message);
        res.status(500).send('Error de servidor.');
    }
});

module.exports = router;