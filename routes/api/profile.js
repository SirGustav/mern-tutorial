const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const { check, validationResult } = require('express-validator');

const Profile = require('../../models/Profile');
const User = require('../../models/User');

//@route    GET api/profile/me
//@desc     Obtener el perfil del usuario actual
//@access   Private
router.get('/me', auth, async (req, res) => {
    try {
        const profile = await Profile.findOne({ user: req.user.id }).populate('user', ['name', 'avatar']);

        if (!profile) {
            return res.status(400).json({ msg: 'No hay un perfil para este usuario.' });
        }

        res.json(profile);
    } catch (e) {
        console.error(e.message);
        res.status(500).send('Error de servidor');
    }
});

//@route    POST api/profile
//@desc     Crear u obtener el perfil de un usuario
//@access   Private
router.post('/', [auth, [
    check('status', 'El estado es obligatorio.').not().isEmpty(),
    check('skills', 'Las habilidades son obligatorias.').not().isEmpty()
]], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    const {
        company, website, location, bio, status, githubusername, skills, youtube, facebook, twitter, instagram, linkedin
    } = req.body;

    //Construir el objeto perfil
    const profileFields = {};
    profileFields.user = req.user.id;
    if (company) profileFields.company = company;
    if (website) profileFields.website = website;
    if (status) profileFields.status = status;
    if (bio) profileFields.bio = bio;
    if (skills) {
        profileFields.skills = skills.split(',').map(skill => skill.trim());
    }

    //Construir el array de rrss
    profileFields.social = {};
    if (youtube) profileFields.social.youtube = youtube;
    if (instagram) profileFields.social.instagram = instagram;
    if (linkedin) profileFields.social.linkedin = linkedin;

    try {
        let profile = await Profile.findOne({ user: req.user.id });

        if (profile) {
            profile = await Profile.findOneAndUpdate({ user: req.user.id }, { $set: profileFields }, { new: true });

            return res.json(profile);
        }

        //Crear
        profile = new Profile(profileFields);

        await profile.save();
        res.json(profile);
    } catch (e) {
        console.error(e.message);
        res.status(500).send('Error de servidor.');
    }
});

//@route    GET api/profile
//@desc     Obtener todos los perfiles
//@access   Public
router.get('/', async (req, res) => {
    try {
        const profiles = await Profile.find().populate('user', ['name', 'avatar']);
        res.json(profiles);
    } catch (e) {
        console.error(err.message);
        res.status(500).send('Error de servidor');
    }
});

//@route    GET api/profile/user/:user_id
//@desc     Obtener perfil por un id
//@access   Public
router.get('/user/:user_id', async (req, res) => {
    try {
        const profile = await Profile.findOne({ user: req.params.user_id }).populate('user', ['name', 'avatar']);

        if (!profile) return res.status(400).json({ msg: 'Perfil no encontrado.' });

        res.json(profile);
    } catch (e) {
        console.error(e.message);
        if (e.kind == 'ObjectId') {
            return res.status(400).json({ msg: 'Perfil no encontrado.' })
        }
        res.status(500).send('Error de servidor');
    }
});

//@route    DELETE api/profile
//@desc     Borrar el perfil, usuario y pots
//@access   Private
router.delete('/', auth, async (req, res) => {
    try {
        // @todo - borrar posts del usuario
        //Borrar perfil
        await Profile.findOneAndRemove({ user: req.user.id });
        //Borrar usuario
        await User.findOneAndRemove({ _id: req.user.id });

        res.json({ msg: 'Usuario eliminado.' });
    } catch (e) {
        console.error(err.message);
        res.status(500).send('Error de servidor');
    }
});

//@route    PUT api/profile/experience
//@desc     Añadir experiencia al perfil
//@access   Private
router.put('/experience', [auth, [
    check('title', 'El título es obligatorio').not().isEmpty(),
    check('company', 'La empresa es obligatoria').not().isEmpty(),
    check('from', 'La fecha de inicio es obligatoria').not().isEmpty()
]], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    const {
        title, company, location, from, to, current, description
    } = req.body;

    const newExp = {
        title, company, location, from, to, current, description
    }

    try {
        const profile = await Profile.findOne({ user: req.user.id });
        //unshift hace lo mismo que push, pero en lugar de insertarlo al final, lo pone al principio. Asi, lo mas reciente queda al principio
        profile.experience.unshift(newExp);
        await profile.save();
        res.json(profile);
    } catch (e) {
        console.error(e.message);
        res.status(500).send('Error de servidor');
    }
});

//@route    DELETE api/profile/experience/:exp_id
//@desc     Eliminar experiencia de un perfil
//@access   Private
router.delete('/experience/:exp_id', auth, async (req, res) => {
    try {
        const profile = await Profile.findOne({ user: req.user.id });

        //Obtener el index a remover
        //Hace un mapeo a traves de la experiencia de un perfil (cada id de la experiencia actual la guarda en la variable item, el cual se va reemplazando), si este concuerda con el exp_id que se encuentra en el header, entonces se guarda 
        const removeIndex = profile.experience.map(item => item.id).indexOf(req.params.exp_id);

        //Sacar solo uno
        profile.experience.splice(removeIndex, 1);

        await profile.save();
        res.json(profile);
    } catch (e) {
        console.error(e.message);
        res.status(500).send('Error de servidor');
    }
});

//@route    PUT api/profile/education
//@desc     Añadir educacion al perfil
//@access   Private
router.put('/education', [auth, [
    check('school', 'La institucuion es obligatoria').not().isEmpty(),
    check('degree', 'El titulo es obligatorio').not().isEmpty(),
    check('fieldofstudy', 'El area es obligatoria').not().isEmpty(),
    check('from', 'La fecha de inicio es obligatoria').not().isEmpty()
]], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    const {
        school, degree, fieldofstudy, from, to, current, description
    } = req.body;

    const newEdu = {
        school, degree, fieldofstudy, from, to, current, description
    }

    try {
        const profile = await Profile.findOne({ user: req.user.id });
        profile.education.unshift(newEdu);
        await profile.save();
        res.json(profile);
    } catch (e) {
        console.error(e.message);
        res.status(500).send('Error de servidor');
    }
});

//@route    DELETE api/profile/education/:edu_id
//@desc     Eliminar educacion de un perfil
//@access   Private
router.delete('/education/:edu_id', auth, async (req, res) => {
    try {
        const profile = await Profile.findOne({ user: req.user.id });

        //Obtener el index a remover
        const removeIndex = profile.education.map(item => item.id).indexOf(req.params.edu_id);

        //Sacar solo uno
        profile.education.splice(removeIndex, 1);

        await profile.save();
        res.json(profile);
    } catch (e) {
        console.error(e.message);
        res.status(500).send('Error de servidor');
    }
});

module.exports = router;