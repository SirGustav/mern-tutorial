const express = require('express');
const connectDB = require('./config/db');

const app = express();

//Conectar la DB
connectDB();

//Inicializar Middleware
app.use(express.json({ extended: false }));

app.get('/', (req, res) => res.send('API corriendo'));

//Definir rutas
app.use('/api/users', require('./routes/api/users'));
app.use('/api/auth', require('./routes/api/auth'));
app.use('/api/profile', require('./routes/api/profile'));
app.use('/api/posts', require('./routes/api/posts'));

//Se guarda en un env ya que Heroku busca esta variable de entorno PORT para la conexion
//Si no hay una variable definida, se conecta al puerto 5000
const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Servidor iniciado en el puerto: ${PORT}`));
