const mongoose = require('mongoose');
const config = require('config');
const db = config.get('mongoURI');

//Configuraciones para quitar los warnings de metodos deprecados
mongoose.set('useNewUrlParser', true);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useFindAndModify', false);

//Async/Await es el nuevo estandar
const connectDB = async () => {
    try {
        await mongoose.connect(db);
        console.log('MongoDB conectado.');
    } catch (e) {
        console.log(e.message);
        //Salir del proceso con un error
        process.exit(1);
    }
}

//En Express al parecer siempre se exporta al final de las clases
module.exports = connectDB;