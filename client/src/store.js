import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import rootReducer from './reducers';

const initialState = {};

const middleware = [thunk];

//createStore que viene de redux, recibe la rootReducer, el initialState, y un middleware. Como estamos ocupando la extension devTools, se puede usar composeWithDevTools, que toma el applyMiddleware, alli se le pasa el middleware con el spread operator (...), el cual en el fondo, divide su contenido y se lo entrega como variables individuales por lo que entiendo
const store = createStore(rootReducer, initialState, composeWithDevTools(applyMiddleware(...middleware)));

export default store;