import React from 'react';
import PropTypes from 'prop-types';
//Cada vez que se quiera interacción de un componente con Redux, ya sea llamando una accion u un estado, hay que usar connect
import { connect } from 'react-redux';

//map() es lo mismo que un foreach, a diferencia de que este retorna algo (un JSX por acada alerta, en este caso)
const Alert = ({ alerts }) => alerts !== null && alerts.length > 0 && alerts.map(alert => (
    <div key={alert.id} className={`alert alert-${alert.alertType}`}>
        {alert.msg}
    </div>
));

Alert.propTypes = {
    alerts: PropTypes.array.isRequired
}

const mapStateToProps = state => ({
    alerts: state.alert
});

//Al importar connect, hay que cambiar el export a:
export default connect(mapStateToProps)(Alert);
